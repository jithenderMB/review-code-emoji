import React, { Component } from "react";

export class eachItem extends Component {
  render() {
    return (
      <li className="listItem">
        <p>{this.props.emojiData.symbol}</p>
        <p className="title">{this.props.emojiData.title}</p>
      </li>
    );
  }
}

export default eachItem;
