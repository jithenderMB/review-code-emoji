import React, { Component } from "react";
import EmojiData from "../data.json";
import EachItem from "./eachItem";

class main extends Component {
  state = {
    data: EmojiData,
    userInput: "",
  };

  handleDisplay = () => {
    let filteredData = this.state.data.filter((emoji) =>
      emoji.title
        .toLocaleLowerCase()
        .includes(this.state.userInput.toLocaleLowerCase())
    );
    if (filteredData) {
      return filteredData;
    }
    return this.state.data;
  };

  handleSearch(event) {
    let inputValue = event.target.value;

    this.setState({
      userInput: inputValue,
    });
  }

  render() {
    return (
      <div>
        <h1>Emoji search</h1>
        <input type="text" onChange={(e) => this.handleSearch(e)} />
        <ul>
          {/* <li>array legth = {this.handleDisplay().length}</li> */}
          {this.handleDisplay().map((emoji) => (
            <EachItem key={emoji.title} emojiData={emoji} />
          ))}
        </ul>
      </div>
    );
  }
}

export default main;
