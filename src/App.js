import './App.css';
import Main from './components/main'
import './styles/main.css'

function App() {
  return (
    <div className="App">
      <Main />
    </div>
  );
}

export default App;
